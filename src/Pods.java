
class Pods {

    Pod[] pods;
    int podsSum = 0;
    int fighters = 0;

    public Pods(int zoneCount) {
        pods = new Pod[zoneCount];
        for (int i = 0; i < zoneCount; i++) {
            pods[i] = null;
        }
    }

    public int count() {
        int cnt = 0;
        for (Pod p : pods) {
            if (p != null) {
                cnt += p.power;
            }
        }
        // pods.values().stream().map((p) -> p.power).reduce(cnt, Integer::sum);
        return cnt;
    }

    public void remind(Zone zone, int power) {
        int zId = zone.zId;
        Pod p = pods[zId];
        if (p != null) {
            if (power == 0) {
                //System.err.printf("DEL %d (%d)\n", zId, -p.power);
                podsSum -= p.power;
                pods[zId] = null;
            } else if (p.power != power) {
                //System.err.printf("UP %d (%d)\n", zId, power - p.power);
                podsSum += power - p.power;
                p.power = power;
            }
        } else if (power != 0) {
            //System.err.printf("NEW %d (%d)\n", zId, power);
            pods[zId] = new Pod(zone, power, Func.Scout);
            podsSum += power;
        }
    }

    void clean() {
        podsSum = 0;
        fighters = 0;
    }
}

enum Func {
    Fighter, Scout
}

class Pod {

    int zId;
    Zone zone;
    int power;
    Func function;

    public Pod(int zId, int power) {
        this.zId = zId;
        this.power = power;
    }

    public Pod(Zone zone, int power, Func func) {
        this.zone = zone;
        this.zId = zone.zId;
        this.power = power;
        this.function = func;
    }

    Pod(int zId, int power, Func func) {
        this.zId = zId;
        this.power = power;
        this.function = func;
    }

    public void changePower(int power) {
        this.power = power;
    }

    public void sendTo(int zone) {
        System.out.printf("%d %d %d ", power, zId, zone);
    }

    public void sendTo(int zone, int num) {
        if (num > 0) {
            System.out.printf("%d %d %d ", num, zId, zone);
        }
    }
}
