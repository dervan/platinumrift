
import java.util.*;

/**
 * Created by drv on 19/06/16.
 */
class Board {

    /* Constant desctption of board */
    Zone myBase;
    Zone opBase;
    int zoneCount;
    Zone[] zones;
    List<Zone> byDist; //All zones sorted by dystance to base.
    int alarmSize = 0;

    public Board(int zoneCount) {
        this.zoneCount = zoneCount;
        this.zones = new Zone[zoneCount];
    }

    void calcDist() {
        LinkedList<Zone> bfs = new LinkedList();
        bfs.add(this.myBase);
        this.myBase.baseDist = 0;
        this.myBase.wayTo[1] = this.myBase.zId;

        while (!bfs.isEmpty()) {
            Zone z = bfs.remove();
            for (Zone n : z.neighbours) {
                if (n.wayTo[1] == -1) {//Not visited
                    n.baseDist = z.baseDist + 1;
                    n.wayTo[1] = z.zId;
                    bfs.add(n);
                }
            }
        }

        byDist = new LinkedList<>();
        byDist.addAll(Arrays.asList(zones));
        Collections.sort(byDist, new Comparator<Zone>() {
            public int compare(Zone s1, Zone s2) {
                return s2.baseDist - s1.baseDist;
            }
        });
    }

    /**
     * Calculate shortest way from all zones to given zone
     *
     * @param zone
     * @param wayId
     */
    void wayTo(Zone zone, int wayId) {
        LinkedList<Zone> bfs = new LinkedList();
        bfs.add(zone);

        while (!bfs.isEmpty()) {
            Zone z = bfs.remove();
            for (Zone n : z.neighbours) {
                if (n.wayTo[wayId] == -1) {//Not visited
                    n.wayTo[wayId] = z.zId;
                    bfs.add(n);

                }
            }
        }
    }

    void wayToOp() {
        wayTo(opBase, 0);
    }

    void addZone(int zoneId, int platinumSource) {
        zones[zoneId] = new Zone(zoneId, this);
    }

    void addLink(int zone1, int zone2) {
        Zone z1, z2;
        z1 = zones[zone1];
        z2 = zones[zone2];
        z1.neighbours.add(z2);
        z2.neighbours.add(z1);
    }

    void update(int zId, int own, int minePods, int oponentsPods, int platinum) {
        zones[zId].setPlatinium(platinum);
        zones[zId].update(minePods, oponentsPods, own);
    }

    void clearWayTo(int i) {
        for(Zone z:zones){
            z.wayTo[i] = -1;
        }
    }
    
    void spread(){
        List<Zone> toCheckList = new LinkedList<Zone>();
        
        System.err.printf("TC: ");
        
        for(Zone z:zones){
            if(z.toCheck && z.myPods == 0){
                System.err.printf("%d ", z.zId);
                for(Zone n:z.neighbours){
                    if(!n.toCheck && !n.visible)toCheckList.add(n);
                }
            }
        }
        System.err.printf("\n");
        for(Zone z:toCheckList){
            System.err.printf("New toCheck: %d\n", z.zId);
            z.toCheck = true;
        }
    }

    void setInvisible(int zId) {
        zones[zId].setInvisible();
    }
}
