import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

class Zone implements Comparable {

    final Board board;
    final int zId;
    final List<Zone> neighbours;

    /* Desctiption of zone used for priority calculation */
    int myPods = 0;
    int opPods = 0;
    int owner = 2;
    int platinium = 1; // By default assume that there is something interesting.
    double priority;
    double treePrio;
    boolean toCheck = false; // It tries toch eck if oponent could take over this zone

    /*  These values are partial sums over a spanning tree with
        root in player's base. */
    int treeOpPods;
    int treeMyPods;
    int treeSize;
    int treeDepth;
    int treeToConq;

    int realMyPods;
    int realOpPods;
    int realSize;
    
    double partialPrio;
    
    /*Values used for navigation */
    int[] wayTo;
    int baseDist = -1;
    boolean visible = false;
    private Set<Zone> toUpdate;

    public Zone(int zId, Board board) {
        this.board = board;
        this.zId = zId;
        this.priority = 1;
        this.neighbours = new LinkedList<>();
        this.toUpdate = null;
        wayTo = new int[3];
        wayTo[0] = -1; // Oponents' base
        wayTo[1] = -1; // My base
        wayTo[2] = -1; // Alarm area.
    }
    
    private void initToUpdate(){
        toUpdate = new HashSet<>();
        List<Zone> tmp = new LinkedList<>();
        
        for (Zone n : neighbours){
            if (n.baseDist > baseDist) {
                toUpdate.add(n);
            }
        }
        for (Zone n : toUpdate){
            for (Zone nn : n.neighbours){
                if (nn.baseDist == baseDist+2) {
                    tmp.add(nn);
                }
            }
        }
        for (Zone n : tmp){
            if(Collections.frequency(tmp, n)>1){
                toUpdate.add(n);
            }
        }
    }
    
    public boolean isDeadend(){
        return (toUpdate.size()==0);
    }

    public void updateTree() {
        
        if(toUpdate==null){
            initToUpdate();
        }
        partialPrio = 0;
        treePrio = priority;
        treeMyPods = myPods;
        treeOpPods = opPods;
        treeDepth = baseDist;
        treeSize = 1;
        
        if (owner == 0 || platinium == 0) {
            treeToConq = 0;
        } else {
            treeToConq = 1;
        }
        /* Counted only on spanning tree */
            for(Zone n: toUpdate){
            
                if(n.baseDist==baseDist+1){
                    //System.err.printf("+ %d ", z.zId);
                    treePrio += n.treePrio;
                    //priority += n.priority;
                    treeMyPods += n.treeMyPods;
                    treeOpPods += n.treeOpPods;
                    treeSize += n.treeSize;
                    treeDepth = Integer.max(treeDepth, n.treeDepth);
                    treeToConq += treeToConq;
                }else{
                    //System.err.printf("- %d ", z.zId);
                    treePrio -= n.treePrio;
                    //priority -= n.priority;
                    treeMyPods -= n.treeMyPods;
                    treeOpPods -= n.treeOpPods;
                    treeSize -= n.treeSize;
                    treeToConq -= treeToConq;
            }
        }
        /*for (Zone n : neighbours) {
            if (n.baseDist > baseDist) {
                 treePrio always means most valuable path starting in this node
                //partialPrio = +n.treePrio, treePrio);
                treePrio += n.partialPrio;
                
                if (n.wayTo[1] == zId) {
                    partialPrio += n.treePrio;
                    treeMyPods += n.treeMyPods;
                    treeOpPods += n.treeOpPods;
                    treeSize += n.treeSize;
                    treeDepth = Integer.max(treeDepth, n.treeDepth);
                    treeToConq += treeToConq;
                }
            }
        }
        partialPrio += priority;
        treePrio += priority;*/
        if (board.myBase == this) {
            treePrio = 0;
            partialPrio = 0;
        }
        
        /*  
        if(baseDist<5){
            System.err.printf("%d (%d) =", treeMyPods,  zId);
            for(Zone z: toUpdate){
                    if(z.baseDist==baseDist+1){
                        System.err.printf("+ %d(%d)", z.treeMyPods, z.zId);
                    }else{
                        System.err.printf("- %d(%d) ", z.treeMyPods, z.zId);
                    }
                    
                }
            System.err.printf("\n");
        }*/
    }
    
    public void setInvisible(){
        this.visible = false;
        if(this.opPods!=0){
            this.toCheck=true;
        }
        if(toCheck){
            priority = 0 * platinium ; //+ (opPods-myPods);
        }
    }

    /* Calculate local weight */
    public void update(int myPods, int opPods, int owner) {
        this.priority = 0;
        this.toCheck = false;
        this.visible = true;
        this.myPods = myPods;
        this.opPods = opPods;
        double podsVal;

        /* Base prio */
        this.priority = platinium ;
        
        /* Special case, lost ownership */
        if(this.owner==0 && owner!=0){
            this.priority *= 3;
            this.owner = owner;
            System.err.printf("High prio - lost zone %d\n", zId);
            return;
        }

        /* Set new owner */
        this.owner = owner;
        if (owner == 0) {
            /* Mine zone */
            podsVal = (opPods - 0.5*myPods)*0.1;
            if(podsVal>0)
                this.priority += podsVal;

        } else if (owner == 1) {
            priority *= 2.0;
            priority += 1;
            /* Take all opponents' zones. Even empty. When you're playing with
                              good player, next zones may be interesting*/
        }

       /* Old algorithm
        if(this.owner==0 && owner!=0){
            this.priority += 3*platinium;
            System.err.printf("High prio - lost (%d - %d) zone %d\n", this.owner, owner, zId);
        }
        this.owner = owner;
        if (owner == 0) {
            priority = Math.abs(- myPods + opPods)*0.1;
            if(opPods!=0){
                priority+=1;
            }
            priority *=platinium;
            
        } else if (owner == 1) {
            priority *= 2.0; // Taking an opponents' zone is double profit.
            priority += 1.0;
            priority += opPods*0.1;
            Take all opponents' zones. When you're playing with
                              good player, next zones may be interesting 
        }
        */
    }

    public void setPlatinium(int plat) {
        /* it will happen only once during game */
        if (plat != this.platinium) {
            System.err.printf("Plat: %d %d -> %d\n", this.zId, this.platinium, plat);
        }
        this.platinium = plat;
    }

    @Override
    public int compareTo(Object t) {
        if (!(t instanceof Zone)) {
            throw new ClassCastException("A Zone object expected.");
        }
        return new Double(((Zone) t).treePrio - treePrio).intValue();
    }

    void update() {
        update(myPods, opPods, owner);
    }
}
