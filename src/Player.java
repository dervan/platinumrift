
import java.util.*;
import static java.util.Collections.sort;

class Player {
    static Board board;
    static Pods pods;
    static int myId;
    public static int round = 0;
    private static Random rand = new Random();

    public static void turnOutput() {
        int oponentPods;
        int podsToGo;
        double randomNumber;
        List<Zone> neighbours;
        int neigboursNumber;
        int initPodsToGo;
        boolean anyway;
        round++;
        int j;
        /* Navigate pods */
        for (Pod p : pods.pods) {
            if (p == null) {
                /* Empty pod. Just in case */
                continue;
            }
            
            neighbours = p.zone.neighbours;
            sort(neighbours);
            neigboursNumber = neighbours.size();
            oponentPods = 0;
            oponentPods = neighbours.stream().map((z) -> z.opPods).reduce(oponentPods, Integer::sum);
            oponentPods += board.zones[p.zId].opPods;

            /* In case of alarm send all pods! */
            if(p.zone.baseDist < board.alarmSize){
                System.err.printf("Alarm! %d -> %d", p.zId, p.zone.wayTo[2]);
                if(p.zone.wayTo[2]!=p.zone.zId){
                    p.sendTo(p.zone.wayTo[2]);
                }
                continue;
            }
            
            /* In case of fight */
            if (p.zone == board.myBase || p.zone.platinium != 0) {
                podsToGo = p.power - oponentPods;
            } else {
                podsToGo = p.power;
            }
            
            /* In case of alarm */
            /*if(p.zone.baseDist < board.alarmSize){
                System.err.printf("Alarm: %d -> %d\n", p.zId, p.zone.wayTo[2]);
                p.sendTo(p.zone.wayTo[2]);
            }*/

            if (podsToGo > 0) {
                int[] goesTo = new int[neigboursNumber];
                double[] targetPrio = new double[neigboursNumber];
                
                    for (j = 0; j < neigboursNumber; j++) {
                        goesTo[j] = 0;
                        Zone n = neighbours.get(j);
                        if (podsToGo > 0
                                && n.owner!=0 && n.priority >= 1) { //Attractive neighbour
                            System.err.printf("Special neighbour (z%d, p%f) -> %d\n", p.zId, n.priority, n.zId);
                            goesTo[j]++;
                            podsToGo--;
                            n.myPods++;
                        }
                    }
                
                if(p.zone.treeMyPods >= p.zone.treeOpPods){
                    /* Check if some very interesting neighbour is here */
                    /* Check if there is no tree which needs a help */
                    for (j = 0; j < neigboursNumber; j++) {
                        Zone n = neighbours.get(j);
                        if (podsToGo > 0
                                && n.treeMyPods < n.treeOpPods && n.treePrio > 2.0 ) {
                            System.err.printf("Special tree (s%d, p%f) %d -> %d\n", n.treeSize, n.treePrio, p.zId, n.zId);
                            goesTo[j]++;
                            podsToGo--;
                            n.myPods++;
                        }
                    }
                }
                initPodsToGo = podsToGo;
                double extraShorten;
                /* Still somebody to send */
                if (podsToGo > 0) {
                    /* Prepare table to random split */
                    Zone n = neighbours.get(0);
                    if(n.myPods!=0){
                        extraShorten = 0.2;
                    }else{
                        extraShorten = 1;
                    }
                        
                   
                    if (n.baseDist > p.zone.baseDist) {
                        targetPrio[0] = (n.treePrio + Integer.max(0, n.treeToConq - 2 * n.treeMyPods))*extraShorten;
                    } else {
                        targetPrio[0] = 0.005 * n.treePrio; // Let's add some randomness;
                    }
                    for (int i = 1; i < neigboursNumber; i++) {
                        n = neighbours.get(i);
                        if(n.myPods!=0){
                            extraShorten = 0.2;
                        }else{
                            extraShorten = 1;
                        }   
                        if (n.baseDist > p.zone.baseDist) {
                            targetPrio[i] = targetPrio[i - 1] + (n.treePrio + Integer.max(0, n.treeToConq - 2 * n.treeMyPods))*extraShorten;
                        } else {
                            targetPrio[i] = targetPrio[i - 1] + 0.005 * n.treePrio;
                        }
                    }

                    /* Neigbourhood is not cool. Let's fight! */
                    if (targetPrio[neigboursNumber - 1] < 1.0 || p.zone.isDeadend()) {
                        System.err.printf("Zone %d is uninteresting: %f\n",
                                p.zId, targetPrio[neigboursNumber - 1]);
                        p.sendTo(p.zone.wayTo[0], podsToGo);
                        podsToGo = 0;
                    }

                    while (podsToGo > 0) {
                        randomNumber = rand.nextDouble() * targetPrio[neigboursNumber - 1];
                        j = 0;
                        while (j < neigboursNumber - 1 && targetPrio[j] < randomNumber) {
                            j++;
                        }
                        randomNumber = rand.nextDouble();
                        if(randomNumber < 1/(double)(goesTo[j]+1)){
                            goesTo[j]++;
                            podsToGo--;
                        }
                    }
                }

                /* On the end move pods */
                for (int i = 0; i < neighbours.size(); i++) {
                    //System.err.printf("zId %d(%d) - %f(%d) ",ngh.get(i).zId,ngh.get(i).baseDist, ngh.get(i).treePrio, tgt[i]);
                    p.sendTo(neighbours.get(i).zId, goesTo[i]);
                    neighbours.get(i).myPods += goesTo[i];
                    neighbours.get(i).update();
                }
            }
        }

        System.out.println("\nWAIT");
    }

    public static void firstInput(Scanner in) {
        int minePods;
        int oponentsPods;
        int myPlatinum = in.nextInt(); // your available Platinum
        pods.clean();
        for (int i = 0; i < board.zoneCount; i++) {
            int zId = in.nextInt(); // this zone's ID
            int ownerId = in.nextInt(); // the player who owns this zone (-1 otherwise)
            int podsP0 = in.nextInt(); // player 0's PODs on this zone
            int podsP1 = in.nextInt(); // player 1's PODs on this zone
            int visible = in.nextInt(); // 1 if one of your units can see this tile, else 0
            int platinum = in.nextInt(); // the amount of Platinum this zone can provide (0 if hidden by fog)
            if (ownerId == myId) {
                board.myBase = board.zones[zId];
            } else if (ownerId != -1) {
                board.opBase = board.zones[zId];
            }

            if (myId == 0) {
                minePods = podsP0;
                oponentsPods = podsP1;

            } else {
                minePods = podsP1;
                oponentsPods = podsP0;
            }

            pods.remind(board.zones[zId], minePods);

            if (ownerId == myId) {
                ownerId = 0;
            } else if (ownerId != -1) {
                ownerId = 1;
            }
            if (visible == 1) {
                board.update(zId, ownerId, minePods, oponentsPods, platinum);
            }else{    
                board.setInvisible(zId);
            }
        }
    }

    public static void turnInput(Scanner in) {
        int minePods;
        int oponentsPods;
        int myPlatinum = in.nextInt();
        Zone oponentMax = null;

        for (int i = 0; i < board.zoneCount; i++) {
            int zId = in.nextInt();     // this zone's ID
            int ownerId = in.nextInt(); // the player who owns this zone (-1 otherwise)
            int podsP0 = in.nextInt();  // player 0's PODs on this zone
            int podsP1 = in.nextInt();  // player 1's PODs on this zone
            int visible = in.nextInt(); // 1 if one of your units can see this tile, else 0
            int platinum = in.nextInt();// the amount of Platinum this zone can provide (0 if hidden by fog)

            if (visible == 1) {
                /* Set ownerships of pods */
                if (myId == 0) {
                    minePods = podsP0;
                    oponentsPods = podsP1;
                } else {
                    minePods = podsP1;
                    oponentsPods = podsP0;
                }
                /* Set ownership of zone */
                if (ownerId == myId) {
                    ownerId = 0;
                } else if (ownerId != -1) {
                    ownerId = 1;
                }
                /* Update pods */
                pods.remind(board.zones[zId], minePods);

                /* Update zones priorities */
                board.update(zId, ownerId, minePods, oponentsPods, platinum);

            }else{    
                board.setInvisible(zId);
            }
        }
        int alarmPodVal = 3;
        Zone alarmZone = null;
        oponentMax = null;
        for (Zone z : board.byDist) {
            z.updateTree();
            if (z.baseDist < 5) {
                if(z.opPods > alarmPodVal){
                    oponentMax = z;
                    /*Target alarm zone is next zone on way to  players' base */
                    alarmZone = board.zones[z.wayTo[1]];
                    alarmPodVal = z.opPods;
                }
                //System.err.printf("Zone %d. depth %d, size %d, Mine %d Op %d, Prio %f\n", z.zId, z.treeDepth, z.treeSize, z.treeMyPods, z.treeOpPods, z.treePrio);
            }
        }
        if(alarmZone!=null){
            // Turn alarm on!
            board.wayTo(alarmZone, 2);
            alarmZone.wayTo[2]=oponentMax.zId;
            board.myBase.wayTo[2]=-1;
            board.alarmSize = alarmZone.baseDist + 1;
        }else{
            // Turn alarm off!
            board.clearWayTo(2);
            board.alarmSize = 0;
        }

    }

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);

        int playerCount = in.nextInt(); // the amount of players (always 2)
        myId = in.nextInt(); // my player ID (0 or 1)
        int zoneCount = in.nextInt(); // the amount of zones on the map
        board = new Board(zoneCount);
        pods = new Pods(zoneCount);
        int linkCount = in.nextInt(); // the amount of links between all zones

        for (int i = 0; i < zoneCount; i++) {
            int zoneId = in.nextInt(); // this zone's ID (between 0 and zoneCount-1)
            int platinumSource = in.nextInt(); // Because of the fog, will always be 0
            board.addZone(zoneId, platinumSource);
        }

        for (int i = 0; i < linkCount; i++) {
            int zone1 = in.nextInt();
            int zone2 = in.nextInt();
            board.addLink(zone1, zone2);
        }

        firstInput(in);
        board.wayToOp();
        board.calcDist();
        for (Zone z : board.byDist) {
            z.updateTree();
        }
        turnOutput();
        // game loop
        while (true) {
            turnInput(in);
            board.spread();
            turnOutput();
        }
    }
}
